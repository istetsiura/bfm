<?php
/**
 * @file msnf-block-step-info.tpl.php
 *
 * Displays information about the current form step as block content.
 *
 * Available variables:
 * - $steps_rendered
 *   Step titles rendered as simple item list.
 */
?>

<div class="steps-wrapper">
  <div class="steps-line"></div>
  <?php print $steps_rendered; ?>
</div>
