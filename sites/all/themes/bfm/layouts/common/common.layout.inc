name = Common BFM layout
description = A 3 column layout with a prominent hero region.
preview = preview.png
template = common-layout

; Regions
regions[branding] = Branding
regions[navigation] = Navigation bar
regions[header] = Header
regions[hero] = Hero
regions[highlighted] = Highlighted
regions[help] = Help
regions[before_content] = Before content
regions[content] = Content
regions[after_content] = After content
regions[sidebar_first] = First sidebar
regions[sidebar_second] = Second sidebar
regions[footer] = Footer

; Stylesheets
stylesheets[all][] = css/layouts/common/common.layout.css
