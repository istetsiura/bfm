<?php

module_load_include('inc', 'omega', 'includes/assets/assets');
module_load_include('inc', 'omega', 'includes/assets/assets.extension');
module_load_include('inc', 'omega', 'includes/assets/assets.settings');

/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * BFM Main theme theme.
 */

/**
 * Implements hook_preprocess_page().
 */
function bfm_preprocess_page(&$vars) {
  $vars['page']['content_width'] = 9;
  /*if (!empty($vars['page']['sidebar_first'])) {
    $vars['page']['content_width'] -= 3;
  }*/
  if (!empty($vars['page']['sidebar_second'])) {
    $vars['page']['content_width'] -= 3;
  }
}

/*
 * Implements hook_preprocess_html().
 */
function bfm_preprocess_html(&$vars) {
  // Add font awesome cdn.
  drupal_add_css('//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.css', array(
    'type' => 'external'
  ));
}