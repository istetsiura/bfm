<?php

/**
 * Implements hook_block_info().
 */
function bfm_common_block_info() {
$blocks = array();
  $blocks['bfm_global_filter'] = array(
    'info' => t('BFM Global Filter'),
  );
  $blocks['bfm_global_search'] = array(
    'info' => t('BFM Global Search'),
  );
  return $blocks;
}
/**
 * Implements hook_block_view().
 */
function bfm_common_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'bfm_global_filter':
      $block['subject'] = '';
      $block['content'] = _bfm_common_get_countries_select_list();
      break;
    case 'bfm_global_search':
      $block['subject'] = '';
      $block['content'] = drupal_get_form('bfm_common_search_form');
      break;
  }
  return $block;
}

function _bfm_common_get_countries_select_list() {

}

function bfm_common_search_form(array $form, array &$form_state) {
  // …
  $form['keys'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter keywords'),
  );
  // If there's only one search in your module, with ID "MODULE".
  // Otherwise, get the proper ID, as assigned in MODULE_list_autocomplete_searches(),
  // for the current search.
  $autocomplete_id = 'bfm_common';
  $search = search_api_autocomplete_search_load($autocomplete_id);

  if ($search && $search->enabled) {
    $search->alterElement($form['keys']);
  }
  // …
  return $form;
}

function bfm_common_search_api_autocomplete_types() {
  $types['bfm_common'] = array(
    'name' => t('BFM common searches'),
    'description' => t('Searches provided by the <em>BFM Common</em> module.'),
    'list searches' => 'bfm_common_list_autocomplete_searches',
    'create query' => 'bfm_common_create_autocomplete_query',
    // OPTIONAL – only if you want additional search-specific options, like for Views:
    'config form' => 'bfm_common_autocomplete_config_form',
  );

  return $types;
}

function bfm_common_list_autocomplete_searches(SearchApiIndex $index) {
  $ret = array();
  // If your module only provides one search, it's simple:
  $ret['bfm_common']['name'] = t('BFM Common search');
  // Otherwise, loop over all defined searches:
  /*foreach (… as $search) {
    // Remember to use a proper prefix to avoid conflicts.
    $id = 'MODULE_' . $search->id;
    $ret[$id]['name'] = $search->label;
    // If the searches have additional options/information:
    $ret[$id]['options']['custom'] = $search->options;
  }*/
  return $ret;
}

function bfm_common_create_autocomplete_query(SearchApiAutocompleteSearch $search, $complete, $incomplete) {
  // Create a search query just like you do in the custom search submit function.
  // If you have this in a separate function, all the better!
  $query = bfm_common_create_query($search->options['custom']);
  $query->keys($complete);
  return $query;
}

function bfm_common_create_query($custom) {
  dsm($custom);
}
