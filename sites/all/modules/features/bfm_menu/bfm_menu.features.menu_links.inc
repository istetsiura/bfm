<?php
/**
 * @file
 * bfm_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function bfm_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_constructing:products/constructing.
  $menu_links['main-menu_constructing:products/constructing'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'products/constructing',
    'router_path' => 'products/%',
    'link_title' => 'Constructing',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_constructing:products/constructing',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_fuel:products/fuel.
  $menu_links['main-menu_fuel:products/fuel'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'products/fuel',
    'router_path' => 'products/%',
    'link_title' => 'Fuel',
    'options' => array(
      'alter' => TRUE,
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_fuel:products/fuel',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_heating:products/heating.
  $menu_links['main-menu_heating:products/heating'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'products/heating',
    'router_path' => 'products/%',
    'link_title' => 'Heating',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_heating:products/heating',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_manufacturing:products/manufacturing.
  $menu_links['main-menu_manufacturing:products/manufacturing'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'products/manufacturing',
    'router_path' => 'products/%',
    'link_title' => 'Manufacturing',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_manufacturing:products/manufacturing',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_services:products/services.
  $menu_links['main-menu_services:products/services'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'products/services',
    'router_path' => 'products/%',
    'link_title' => 'Services',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_services:products/services',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: user-menu_log-out:user/logout.
  $menu_links['user-menu_log-out:user/logout'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user/logout',
    'router_path' => 'user/logout',
    'link_title' => 'Log out',
    'options' => array(
      'identifier' => 'user-menu_log-out:user/logout',
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-sign-out',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: user-menu_login:user/login.
  $menu_links['user-menu_login:user/login'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user/login',
    'router_path' => 'user/login',
    'link_title' => 'Login',
    'options' => array(
      'identifier' => 'user-menu_login:user/login',
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-sign-in',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: user-menu_post-free-ad:node/add/bfm-product.
  $menu_links['user-menu_post-free-ad:node/add/bfm-product'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'node/add/bfm-product',
    'router_path' => 'node/add/bfm-product',
    'link_title' => 'Post Free Ad',
    'options' => array(
      'identifier' => 'user-menu_post-free-ad:node/add/bfm-product',
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-plus',
          2 => 'post-fee-add',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: user-menu_sign-up:user/register.
  $menu_links['user-menu_sign-up:user/register'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user/register',
    'router_path' => 'user/register',
    'link_title' => 'Sign Up',
    'options' => array(
      'identifier' => 'user-menu_sign-up:user/register',
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-user-plus',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: user-menu_user-account:user.
  $menu_links['user-menu_user-account:user'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user',
    'router_path' => 'user',
    'link_title' => 'User account',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'user-menu_user-account:user',
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-user',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Constructing');
  t('Fuel');
  t('Heating');
  t('Log out');
  t('Login');
  t('Manufacturing');
  t('Post Free Ad');
  t('Services');
  t('Sign Up');
  t('User account');

  return $menu_links;
}
