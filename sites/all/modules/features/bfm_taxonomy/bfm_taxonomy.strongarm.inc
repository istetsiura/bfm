<?php
/**
 * @file
 * bfm_taxonomy.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function bfm_taxonomy_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_bfm_briquette_type_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_bfm_briquette_type_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_bfm_category_pattern';
  $strongarm->value = 'products/category/[term:name]';
  $export['pathauto_taxonomy_term_bfm_category_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_bfm_kind_of_wood_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_bfm_kind_of_wood_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_bfm_raw_materials_liquid_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_bfm_raw_materials_liquid_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_bfm_raw_materials_solid_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_bfm_raw_materials_solid_pattern'] = $strongarm;

  return $export;
}
