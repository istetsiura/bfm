<?php
/**
 * @file
 * bfm_product.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function bfm_product_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'bfm_facets_pellets';
  $context->description = '';
  $context->tag = 'product facets';
  $context->conditions = array(
    'taxonomy_term' => array(
      'values' => array(
        'bfm_category' => 'bfm_category',
      ),
      'options' => array(
        'term_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'facetapi-sOQ0d5XOYU5LuZyuOCK4N072Fz1dHzf5' => array(
          'module' => 'facetapi',
          'delta' => 'sOQ0d5XOYU5LuZyuOCK4N072Fz1dHzf5',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'facetapi-XgOE9yH4CTPXoccYMXWzKI1uDh6sn0to' => array(
          'module' => 'facetapi',
          'delta' => 'XgOE9yH4CTPXoccYMXWzKI1uDh6sn0to',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'facetapi-EDcP09oMbaP6J3ceyUDQoCXy736ypIyI' => array(
          'module' => 'facetapi',
          'delta' => 'EDcP09oMbaP6J3ceyUDQoCXy736ypIyI',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('product facets');
  $export['bfm_facets_pellets'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'bfm_product_form';
  $context->description = '';
  $context->tag = 'form';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'bfm_product' => 'bfm_product',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        'node/add/bfm-product' => 'node/add/bfm-product',
        'node/*/edit' => 'node/*/edit',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'msnf-msnf_step_data' => array(
          'module' => 'msnf',
          'delta' => 'msnf_step_data',
          'region' => 'before_content',
          'weight' => '-10',
        ),
      ),
      'layout' => 'common',
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('form');
  $export['bfm_product_form'] = $context;

  return $export;
}
