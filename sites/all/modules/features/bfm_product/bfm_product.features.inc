<?php
/**
 * @file
 * bfm_product.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bfm_product_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function bfm_product_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_search_api_index().
 */
function bfm_product_default_search_api_index() {
  $items = array();
  $items['bfm_products_index'] = entity_import('search_api_index', '{
    "name" : "Products Index",
    "machine_name" : "bfm_products_index",
    "description" : null,
    "server" : "bfm_solr",
    "item_type" : "node",
    "options" : {
      "datasource" : { "bundles" : [ "bfm_product" ] },
      "index_directly" : 1,
      "cron_limit" : "50",
      "fields" : {
        "field_bfm_ash_content" : { "type" : "decimal" },
        "field_bfm_ask_bid" : { "type" : "integer" },
        "field_bfm_calorific_value" : { "type" : "decimal" },
        "field_bfm_category_ref" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_bfm_chlorine" : { "type" : "decimal" },
        "field_bfm_fc_images" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "field_collection_item"
        },
        "field_bfm_fc_product_addr" : { "type" : "integer", "entity_type" : "field_collection_item" },
        "field_bfm_humidity" : { "type" : "decimal" },
        "field_bfm_rm_liquid_ref" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_bfm_rm_solid_ref" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_bfm_size_mm" : { "type" : "decimal" },
        "field_bfm_sulfur" : { "type" : "decimal" },
        "field_bfm_unit" : { "type" : "integer" },
        "field_bfm_unit_per_month" : { "type" : "decimal" },
        "language" : { "type" : "string" },
        "nid" : { "type" : "integer" },
        "search_api_language" : { "type" : "string" },
        "title_field" : { "type" : "text" },
        "url" : { "type" : "uri" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-10",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "fields" : { "title_field" : true } }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "title_field" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "title_field" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title_field" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}
