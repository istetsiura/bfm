<?php
/**
 * @file
 * bfm_config.msnf.inc
 */

/**
 * Implements hook_msnf_step_info().
 */
function bfm_config_msnf_step_info() {
  $export = array();

  $msnf = new stdClass();
  $msnf->disabled = FALSE; /* Edit this to true to make a default msnf disabled initially */
  $msnf->api_version = 1;
  $msnf->identifier = 'step_bfm_address|node|bfm_product';
  $msnf->step_name = 'step_bfm_address';
  $msnf->entity_type = 'node';
  $msnf->bundle = 'bfm_product';
  $msnf->data = array(
    'label' => 'Address',
    'weight' => '6',
    'children' => array(
      0 => 'field_bfm_fc_product_addr',
    ),
    'format_type' => 'default',
    'format_settings' => array(
      'label' => 'Address',
      'instance_settings' => array(
        'skip_non_required' => 0,
        'hide_if_empty' => 0,
        'classes' => 'step-not-last step-address',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'buttons' => array(
          'previous' => 'Back',
          'next' => 'Next',
          'skip' => 'Skip next step',
        ),
      ),
    ),
  );
  $export['step_bfm_address|node|bfm_product'] = $msnf;

  $msnf = new stdClass();
  $msnf->disabled = FALSE; /* Edit this to true to make a default msnf disabled initially */
  $msnf->api_version = 1;
  $msnf->identifier = 'step_bfm_description|node|bfm_product';
  $msnf->step_name = 'step_bfm_description';
  $msnf->entity_type = 'node';
  $msnf->bundle = 'bfm_product';
  $msnf->data = array(
    'label' => 'Description',
    'weight' => '3',
    'children' => array(
      0 => 'body',
      1 => 'title_field',
    ),
    'format_type' => 'default',
    'format_settings' => array(
      'label' => 'Description',
      'instance_settings' => array(
        'skip_non_required' => 0,
        'hide_if_empty' => 0,
        'classes' => 'step-not-last step-description',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'buttons' => array(
          'previous' => 'Back',
          'next' => 'Next',
          'skip' => 'Skip next step',
        ),
      ),
    ),
  );
  $export['step_bfm_description|node|bfm_product'] = $msnf;

  $msnf = new stdClass();
  $msnf->disabled = FALSE; /* Edit this to true to make a default msnf disabled initially */
  $msnf->api_version = 1;
  $msnf->identifier = 'step_bfm_pictures|node|bfm_product';
  $msnf->step_name = 'step_bfm_pictures';
  $msnf->entity_type = 'node';
  $msnf->bundle = 'bfm_product';
  $msnf->data = array(
    'label' => 'Pictures',
    'weight' => '7',
    'children' => array(
      0 => 'field_bfm_fc_images',
    ),
    'format_type' => 'default',
    'format_settings' => array(
      'label' => 'Pictures',
      'instance_settings' => array(
        'skip_non_required' => 0,
        'hide_if_empty' => 0,
        'classes' => 'step-last step-pictures',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'buttons' => array(
          'previous' => 'Back',
          'next' => 'Next',
          'skip' => 'Skip next step',
        ),
      ),
    ),
  );
  $export['step_bfm_pictures|node|bfm_product'] = $msnf;

  $msnf = new stdClass();
  $msnf->disabled = FALSE; /* Edit this to true to make a default msnf disabled initially */
  $msnf->api_version = 1;
  $msnf->identifier = 'step_bfm_product_type|node|bfm_product';
  $msnf->step_name = 'step_bfm_product_type';
  $msnf->entity_type = 'node';
  $msnf->bundle = 'bfm_product';
  $msnf->data = array(
    'label' => 'Type',
    'weight' => '2',
    'children' => array(
      0 => 'field_bfm_category_ref',
      1 => 'field_bfm_ask_bid',
    ),
    'format_type' => 'default',
    'format_settings' => array(
      'label' => 'Type',
      'instance_settings' => array(
        'skip_non_required' => 0,
        'hide_if_empty' => 0,
        'classes' => 'step-first step-not-last step-type',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'buttons' => array(
          'previous' => 'Back',
          'next' => 'Next',
          'skip' => '',
        ),
      ),
    ),
  );
  $export['step_bfm_product_type|node|bfm_product'] = $msnf;

  $msnf = new stdClass();
  $msnf->disabled = FALSE; /* Edit this to true to make a default msnf disabled initially */
  $msnf->api_version = 1;
  $msnf->identifier = 'step_bfm_specifications|node|bfm_product';
  $msnf->step_name = 'step_bfm_specifications';
  $msnf->entity_type = 'node';
  $msnf->bundle = 'bfm_product';
  $msnf->data = array(
    'label' => 'Specifications',
    'weight' => '4',
    'children' => array(
      0 => 'field_bfm_rm_solid_ref',
      1 => 'field_bfm_rm_liquid_ref',
      2 => 'field_bfm_unit_per_month',
      3 => 'field_bfm_unit',
      4 => 'field_bfm_size_mm',
      5 => 'field_bfm_calorific_value',
      6 => 'field_bfm_ash_content',
      7 => 'field_bfm_humidity',
      8 => 'field_bfm_sulfur',
      9 => 'field_bfm_chlorine',
    ),
    'format_type' => 'default',
    'format_settings' => array(
      'label' => 'Specifications',
      'instance_settings' => array(
        'skip_non_required' => 0,
        'hide_if_empty' => 0,
        'classes' => 'step-not-last step-specifications',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'buttons' => array(
          'previous' => 'Back',
          'next' => 'Next',
          'skip' => 'Skip next step',
        ),
      ),
    ),
  );
  $export['step_bfm_specifications|node|bfm_product'] = $msnf;

  return $export;
}
