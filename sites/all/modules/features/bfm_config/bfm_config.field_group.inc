<?php
/**
 * @file
 * bfm_config.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function bfm_config_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_del_available_to|node|bfm_product|form';
  $field_group->group_name = 'group_del_available_to';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bfm_product';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Delivery available to',
    'weight' => '1',
    'children' => array(),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Delivery available to',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-del-available-to field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_del_available_to|node|bfm_product|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Delivery available to');

  return $field_groups;
}
