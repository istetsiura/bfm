<?php
/**
 * @file
 * bfm_config.features.language.inc
 */

/**
 * Implements hook_locale_default_languages().
 */
function bfm_config_locale_default_languages() {
  $languages = array();

  // Exported language: en.
  $languages['en'] = array(
    'language' => 'en',
    'name' => 'English',
    'native' => 'English',
    'direction' => 0,
    'enabled' => 1,
    'plurals' => 0,
    'formula' => '',
    'domain' => '',
    'prefix' => '',
    'weight' => 0,
  );
  // Exported language: ru.
  $languages['ru'] = array(
    'language' => 'ru',
    'name' => 'Russian',
    'native' => 'Русский',
    'direction' => 0,
    'enabled' => 1,
    'plurals' => 0,
    'formula' => '',
    'domain' => '',
    'prefix' => 'ru',
    'weight' => 0,
  );
  // Exported language: uk.
  $languages['uk'] = array(
    'language' => 'uk',
    'name' => 'Ukrainian',
    'native' => 'Українська',
    'direction' => 0,
    'enabled' => 1,
    'plurals' => 0,
    'formula' => '',
    'domain' => '',
    'prefix' => 'uk',
    'weight' => 0,
  );
  return $languages;
}
