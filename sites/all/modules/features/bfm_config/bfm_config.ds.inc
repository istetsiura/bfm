<?php
/**
 * @file
 * bfm_config.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function bfm_config_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'taxonomy_term|bfm_category|bfm_teaser';
  $ds_layout->entity_type = 'taxonomy_term';
  $ds_layout->bundle = 'bfm_category';
  $ds_layout->view_mode = 'bfm_teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_bfm_icon',
        1 => 'name_field',
        2 => 'description_field',
      ),
    ),
    'fields' => array(
      'field_bfm_icon' => 'ds_content',
      'name_field' => 'ds_content',
      'description_field' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['taxonomy_term|bfm_category|bfm_teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'taxonomy_term|bfm_category|default';
  $ds_layout->entity_type = 'taxonomy_term';
  $ds_layout->bundle = 'bfm_category';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_bfm_icon',
        1 => 'name_field',
        2 => 'description_field',
      ),
    ),
    'fields' => array(
      'field_bfm_icon' => 'ds_content',
      'name_field' => 'ds_content',
      'description_field' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['taxonomy_term|bfm_category|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'taxonomy_term|bfm_category|full';
  $ds_layout->entity_type = 'taxonomy_term';
  $ds_layout->bundle = 'bfm_category';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'name_field',
        1 => 'description_field',
        2 => 'field_bfm_icon',
      ),
    ),
    'fields' => array(
      'name_field' => 'ds_content',
      'description_field' => 'ds_content',
      'field_bfm_icon' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['taxonomy_term|bfm_category|full'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function bfm_config_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'bfm_teaser';
  $ds_view_mode->label = 'Teaser';
  $ds_view_mode->entities = array(
    'node' => 'node',
    'taxonomy_term' => 'taxonomy_term',
    'comment' => 'comment',
    'field_collection_item' => 'field_collection_item',
    'user' => 'user',
  );
  $export['bfm_teaser'] = $ds_view_mode;

  return $export;
}
